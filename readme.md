# Awesome WordPress
List of Wordpress stuff you must have

- Awesome WordPress
    - [Core](#core)
    - [Utils](#utils)
    - [Themes](#themes)
    - [Plugins](#plugins)
    
## Core

## Utils
* [underscores](http://underscores.me/) Wordpress theme boilerplate

## Themes

## Plugins
* [Regenerate Thumbnails](https://wordpress.org/plugins/regenerate-thumbnails/) Regenerates thumbnails (good for when changing thumbnail sizes).
* [Contact Form 7](https://wordpress.org/plugins/contact-form-7/) Powerfull contact form engine. 
* [Contact Form DB](https://wordpress.org/plugins/contact-form-7-to-database-extension/) Captchers all submited contact forms from many contact form plugins. 
* [Intuitive Custom Post Order](https://wordpress.org/plugins/intuitive-custom-post-order/) Ridiculously Powerfull tool for reordering posts.
* [User Role Editor](https://wordpress.org/plugins/user-role-editor/) Add/change user roles and capabilities.
* [WP-Cache.com](https://wordpress.org/plugins/wp-cachecom/) Powerfull Cacher.
* [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/).
* [CMB2](https://wordpress.org/plugins/cmb2/) A Powerfull API for creating metaboxs (Ready To Go).
* [Redirection](https://wordpress.org/plugins/redirection/) Powerfull redirection.
* [Automatic Domain Changer](https://wordpress.org/plugins/automatic-domain-changer/) Change domain name in Database.
* [TinyMCE Advanced](https://wordpress.org/plugins/tinymce-advanced) Adds option to the content wysiwyg.
* [Maintenance](https://wordpress.org/plugins/maintenance/) Under construction/Maintenance mode.
### best of evil
* [Duplicator](https://wordpress.org/plugins/duplicator/) clone a wordpres with all content with option to change domain (good for moving a wordpress between servers and domains).